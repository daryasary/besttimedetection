from utils import get_next_link, get_previous_link


def paginate_data(data, count, limit, offset, request):
	# count = len(data)
	# if offset < count:
	# 	data = data[offset: limit+offset]
	return dict(
		count=count, 
		next=get_next_link(count, limit, offset, request) if (count>limit+offset) else None, 
		previous=get_previous_link(count, limit, offset, request), 
		results=data
	)
