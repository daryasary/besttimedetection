import json
import logging
import configs

from bson import ObjectId

from pymongo import MongoClient

from pymongo.errors import BulkWriteError, DuplicateKeyError

from utils import bson2dict, filter_by_service, is_constant


def init_mongo(**kwargs):
    # TODO: Check if custom mongo configurations passed to function
    # client = MongoClient(configs.MONGO_CONFIGURATION)
    # mongo_db = client[configs.MONGO_DB]

    connection = MongoClient(configs.MONGO_HOST)
    mongo_db = connection[configs.MONGO_DB]
    if configs.MONGO_USER is not None:
        mongo_db.authenticate(configs.MONGO_USER, configs.MONGO_PASS)
    return mongo_db


class BaseMongoHandler:
    def __init__(self, **kwargs):
        self.db = init_mongo(**kwargs)

    def get_intended_scope(self, collection):
        return getattr(self.db, collection)

    def get_collections_list(self):
        return self.db.collection_names()

    def get_objects_list(self, collection, query=None, sort=None, skip=0, limit=0):
        intended = self.get_intended_scope(collection)
        
        if query is None:
            query = dict()

        if isinstance(query, dict):
            query = [query]

        if sort is None:
            result = intended.find(*query).skip(skip).limit(limit)

        else:
            result = intended.find(*query).sort(sort).skip(skip).limit(limit)

        return result, result.count()

    def get_objects_count(self, collection, query=None):
        if query is None:
            query = dict()

        return self.get_objects_list(collection, query)[1]

    def get_object(self, collection, query=None):
        intended = self.get_intended_scope(collection)

        if query is None:
            query = dict()

        if isinstance(query, dict):
            query = [query]
        return bson2dict(intended.find_one(*query))

    def save_object(self, collection, data):
        intended = self.get_intended_scope(collection)
        inserted = intended.insert_one(data)
        return bson2dict(intended.find_one({'_id': inserted.inserted_id}))

    def delete_object(self, collection, query):
        data = self.get_object(collection, query)
        if is_constant(data):
            response = ConstantResponse.objects.get(object_id=data['_id'])
            response.delete()
        intended = self.get_intended_scope(collection)
        return intended.delete_one(query)

    def save_list(self, collection, data):
        intended = self.get_intended_scope(collection)

        try:
            inserted = intended.insert_many(data)
            ids = [str(i) for i in inserted.inserted_ids]
        except BulkWriteError:
            # This error means there is an error inside a list and
            # objects should been saved separately one by one
            ids = list()
            for d in data:
                try:
                    ids.append(str(intended.insert_one(d).inserted_id))
                except DuplicateKeyError:
                    # This dict is previously saved so continue for loop
                    ids.append(str(intended.find_one(d)['_id']))
        return ids

    def update_list(self, collection, query, data):
        intended = self.get_intended_scope(collection)
        updated = intended.update_many(query, data)
        return {
            "matched_count": updated.matched_count,
            "modified_count": updated.modified_count
        }


def load_users(tag, offset, limit):
    db = BaseMongoHandler()
    result = db.get_objects_list('qualified_times', {'best_time_1': tag}, skip=offset, limit=limit)
    count = db.get_objects_count('qualified_times', {'best_time_1': tag})
    return result, count


def load_tags(phone_number):
    db = BaseMongoHandler()
    result = db.save_list('qualified_times', {'_phone_number': phone_number})
    return result
