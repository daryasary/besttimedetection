from flask import jsonify
import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from utils import clean_phone_number
from pagination import paginate_data
from mongo import load_users, load_tags


bp = Blueprint('api', __name__, url_prefix='/api')


@bp.route('/', methods=('GET',))
def handler():
	phone_number = request.args.get('user')
	tag = request.args.get('tag')
	if phone_number is not None:
		user = clean_phone_number(phone_number)
		response = {
		'msg': 'Will show data for user: {}'.format(user), 
		'args': request.args, 'url': request.url
		}
	elif tag is not None:
		limit = int(request.args.get('limit', 100))
		offset = int(request.args.get('offset', 0))
		data, _count = load_users(tag, offset, limit)
		response = paginate_data(data, _count, limit, offset, request)
	else:
		response = {
		'msg': 'Will print help message', 
		'args': request.args, 'url': request.url
		}
	return jsonify(response)
