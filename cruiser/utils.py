def clean_phone_number(phone_number):
    phone_number = str(phone_number)
    return '98' + phone_number[len(phone_number)-10:]


def get_next_link(total, limit, offset, request):
	return request.base_url + '?tag={}&limit={}&offset={}'.format(request.args['tag'], limit, offset+limit)


def get_previous_link(total, limit, offset, request):
	if offset-limit < 0:
		return None
	return request.base_url + '?tag={}&limit={}&offset={}'.format(request.args['tag'], limit, offset-limit)
