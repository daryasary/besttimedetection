import logging
import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt

from configs import *
from loaders import load_data

from datetime import datetime

from mongo import save_segments, save_tags, save_collection


def find_send_hour(x):
	return datetime.strftime(x['created_time'], "%H:%M:%S")


def phone_numbers_generators(grouped_data):
	for grp_name in grouped_data.groups.keys():
		yield grp_name

def calculate_score(created_times):
	# return count of user sent_logs for each list
	s1, s2, s3, s4, s5, s6 = 0, 0, 0, 0, 0, 0
	for created_time in created_times:
		if created_time.hour in range(4):
			s1 += 1
		elif created_time.hour in range(4, 8):
			s2 += 1 
		elif created_time.hour in range(8, 12):
			s3 += 1 
		elif created_time.hour in range(12, 16):
			s4 += 1 
		elif created_time.hour in range(16, 20):
			s5 += 1 
		elif created_time.hour in range(20, 24):
			s6 += 1 
	return [s1, s2, s3, s4, s5, s6]


def get_wight(price):
	return price//100


def calculate_weight(user_group):
	w1, w2, w3, w4, w5, w6 = 0, 0, 0, 0, 0, 0

	# user_group['weight_value'] = user_group.apply(lambda x: (x[price]//1000), axis=1)
	for row_tuple in user_group.iterrows():
		row = row_tuple[1]
		if row['created_time'].hour in range(4):
			w1 += row['price']//1000
		elif row['created_time'].hour in range(4, 8):
			w2 += row['price']//1000
		elif row['created_time'].hour in range(8, 12):
			w3 += row['price']//1000
		elif row['created_time'].hour in range(12, 16):
			w4 += row['price']//1000
		elif row['created_time'].hour in range(16, 20):
			w5 += row['price']//1000
		elif row['created_time'].hour in range(20, 24):
			w6 += row['price']//1000
	return [w1, w2, w3, w4, w5, w6]


def generate_user_data(phone_number, user_group):
	print("#### Get data for: ", phone_number)
	return [phone_number] + calculate_score(user_group['created_time']) + calculate_weight(user_group)


def get_score_tag(row):
	# In CSV format
	created_time = pd.to_datetime(row.created_time)
	# In SQL Format
	# created_time = row['created_time']
	if created_time.hour in range(4):
		tag = 's_1'
	elif created_time.hour in range(4, 8):
		tag = 's_2'
	elif created_time.hour in range(8, 12):
		tag = 's_3'
	elif created_time.hour in range(12, 16):
		tag = 's_4'
	elif created_time.hour in range(16, 20):
		tag = 's_5'
	elif created_time.hour in range(20, 24):
		tag = 's_6'
	return tag


def generate_user_data_vectorized(phone_number, user_data):
	stats = user_data.groupby("score_tag").agg({"score_tag": np.count_nonzero, "price":np.sum})
	# score_count = user_data.groupby("score_tag")["score_tag"].count()
	# weight_sum = user_data.groupby("score_tag")["price"].sum()
	stats.rename(columns={'price': 'weight_value'}, inplace=True)
	stats['weight_value'] = stats['weight_value']//1000
	df_out = stats.stack()
	df_out['phone_number'] = phone_number
	df_out.index = df_out.index.map('{0[1]}_{0[0]}'.format)
	return df_out


def dump_to_mongo(source, collection, limit=None, **kwargs):
	df = load_data(load_type='csv', path=source, limit=limit)
	# result.to_csv('statics/output_vectorized.csv', index=False)
	save_collection(collection, df.T.to_dict().values())


def dump_to_csv(destination, limit=None, **kwargs):
	df = load_data(load_type='sql', limit=limit)
	df.to_csv(destination, index=False)
	

def show_variety(by_hours=True, by_minutes=False, loader='csv', limit=None, **kwargs):
	# GET CORRESPONED DATA IN ORDER TO SEND DATA
	# axis=1 force pandas to iterate over rows and send each row as args to the function
	# axis=0 force pandas to iterate over columns ...
	df = load_data(load_type=loader, limit=limit)
	# df['send_hour'] = df.apply(find_send_hour, axis=1)
	times = pd.DatetimeIndex(df.created_time)
	# print df.head()
	if by_minutes:
		grouped_df = df.groupby([times.hour, times.minute]).id.count()

	if by_hours:
		grouped_df = df.groupby([times.hour]).id.count()
	plt.figure()
	grouped_df.plot(kind='bar')
	# plt.fmt_xdata()
	plt.show()


def rate_segments(loader='csv', limit=None, **kwargs):
	df = load_data(load_type=loader, limit=limit)

	# Aggregate send logs data (group by) user's phone_number and calculate score for each one
	logging.info('Do process using pandas')
	df['score_tag'] = df.apply(get_score_tag, axis=1)

	grouped_by_user = df.groupby('phone_number')

	frames = list()
	counter = len(grouped_by_user)
	for phone_number in phone_numbers_generators(grouped_by_user):
		logging.debug('Remain: {}{}Current: {}'.format(counter,' '*(12-len(str(counter))) ,phone_number))
		frames.append(generate_user_data_vectorized(phone_number, grouped_by_user.get_group(phone_number)))
		counter -= 1
		
	columns = ['phone_number', 'score_1', 'score_2', 'score_3', 'score_4', 'score_5', 'score_6', 'weight_1', 'weight_2', 'weight_3', 'weight_4', 'weight_5', 'weight_6']
	result = pd.concat(frames, axis=1).T
	result.fillna(0, inplace=True)
	logging.debug('\n{}'.format(result.count()))
	logging.debug('\n{}'.format(result.tail()))

	result.to_csv('statics/output_vectorized.csv', index=False)
	# save_segments(result.T.to_dict().values())

	# Samething in pythonic way
	# grouped_by_user = df.groupby('phone_number')
	# nd = np.array([[0,0,0,0,0,0,0,0,0,0,0,0,0]])

	# for phone_number in phone_numbers_generators(grouped_by_user):
	# 	nd = np.append(nd, [generate_user_data(phone_number, grouped_by_user.get_group(phone_number))], axis=0)

	# columns = ['phone_number', 'score_1', 'score_2', 'score_3', 'score_4', 'score_5', 'score_6', 'weight_1', 'weight_2', 'weight_3', 'weight_4', 'weight_5', 'weight_6']

	# users_score = pd.DataFrame(nd[1:], columns=columns)
	# users_score.to_csv('output_vectorized.csv', index=False, header=columns)



def qualify_times(limit=None, **kwargs):
	headers = ['phone_numbers', 'best_time_1', 'best_time_2', 'best_time_3', 'best_time_4']
	def analyze_tags(row):
	    values = row.to_dict()
	    # values = list(row.to_dict('index').values())[0]
	    phone_number = values.pop('_phone_number')
	    tags = dict([(key, values[key]) for key in values if 'score_tag_s_' in key])
	    weight = dict([(key, values[key]) for key in values if 'weight_value_s_' in key])
	    sorted_tags = sorted(tags.items(), key=lambda kv: kv[1], reverse=True)
	    if len(sorted_tags) < 2:
	   		return pd.Series([phone_number, sorted_tags[0][0][-3:], None, None, None] , index=headers)
	    # sorted_weights = dict(sorted(weight.items(), key=lambda kv: kv[1], reverse=True))
	    best_times = list()
	    total_scores = sum(tags.values())
	    while len(best_times) < 4:
	        if sum([s[1] for s in sorted_tags]) < (total_scores / 2):
	            best_times.extend((4 - len(best_times)) * [None])
	            break
	        
	        tag_1, tag_2 = sorted_tags[0], sorted_tags[1]
	        # If substraction of tags is more than 25% of tag_1
	        if abs(tag_1[1] - tag_2[1]) > (tag_1[1] / 4):
	            best_times.append(tag_1[0][-3:])
	            sorted_tags.pop(0)
	            continue
	        
	        # Find related weights
	        weight_1, weight_2 = weight['weight_value_' + tag_1[0][-3:]], weight['weight_value_' + tag_2[0][-3:]]
	        if abs(weight_1 - weight_2) > (weight_1 / 4):
	            if weight_2 > weight_1:
	                best_times.append(tag_2[0][-3:])
	                sorted_tags.pop(0)
	                continue
	            else:
	                best_times.append(tag_1[0][-3:])
	                sorted_tags.pop(0)
	                continue
	        
	        best_times.append(tag_1[0][-3:])
	        sorted_tags.pop(0)
	        if len(best_times) < 4:
	            best_times.append(tag_2[0][-3:])
	            sorted_tags.pop(0)

	    return pd.Series([phone_number] + best_times, index=headers)
    
	df = pd.read_csv('statics/output_vectorized-complete.csv', nrows=limit)
	result = df.apply(analyze_tags, axis=1)
	result.to_csv('statics/best_time_result.csv', index=False)
	# save_tags(result.T.to_dict().values())
