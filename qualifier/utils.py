def bson2dict(data):
    if data:
        for k, v in data.items():
            if isinstance(v, ObjectId):
                data[k] = str(v)
        return data
    return {}


def filter_by_service(collections, services):
    # collections = extract_service_names(collections)
    return filter(lambda col: col.split('_')[0] in services, collections)


def is_constant(data):
    if '_device_uuid' not in data.keys():
        return data['_device_uuid'] is None
    return False