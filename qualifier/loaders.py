import MySQLdb

import logging
import pandas as pd

from configs import *


def create_connection():
    logging.info('Creating MySQL connection')
    connection= MySQLdb.connect(**MYSQL_CONNECTION)
    return connection


def close_connection(connection):
	logging.info('Closing MySQL connection')
	connection.close()


def load_data_from_mysql(query=DEFAULT_QUERY, limit=None):
	connection = create_connection()
	if limit is not None:
		logging.info('Loading data from mysql with limit {}'.format(limit))
		df = pd.read_sql(LIMITTED_QUERY.format(limit), con=connection)
	else:
		logging.info('Loading data from mysql with no limit')
		df = pd.read_sql(DEFAULT_QUERY, con=connection)
	close_connection(connection)
	return df


def load_data_from_csv(path=DEFAULT_PATH, limit=None):
	if limit is not None:
		logging.info('Loading data from csv with limit {}'.format(limit))
		df = pd.read_csv(path, nrows=limit)
	else:
		logging.info('Loading data from csv with no limit')
		df = pd.read_csv(path)
	return df


def load_data(load_type, limit=None, **kwargs):
	if load_type == 'csv':
		df = load_data_from_csv(limit=limit, **kwargs)
	elif load_type == 'sql':
		df = load_data_from_mysql(limit=limit, **kwargs)
	else:
		df = None
	return df
