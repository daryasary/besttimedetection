from local_configs import *


MYSQL_CONNECTION = dict(
    host=DB_HOST, user=DB_USER, passwd=DB_PASSWORD, db=DB_NAME
)

DEFAULT_QUERY = 'SELECT payments.phone_number, payments_logs.id, ' \
                'payments_logs.created_time, payments_logs.result_parsed, ' \
                'payments_logs.p_duration, payments_logs.price, ' \
                'payments.status, payments.auto_recharge, ' \
                'payments.aggregator, payments.is_renew FROM ' \
                'send_logs.payments_logs INNER JOIN send_logs.payments ' \
                'WHERE payments.id = payments_logs.payment_id;'

LIMITTED_QUERY = DEFAULT_QUERY[:-1] + 'limit {};'

DEFAULT_PATH = 'statics/payments-export-2018-09-22.csv'