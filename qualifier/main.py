import sys
import argparse
import logging

from modules import show_variety, rate_segments, qualify_times, dump_to_mongo, dump_to_csv


def parse_arguments():
	"""
	python main.py rate_segments --limit 100

	python main.py qualify_times --limit 100

	python main.py dump_to_mongo --collection qualified_times --source statics/best_time_result.csv
	
	python main.py dump_to_csv --destination statics/test.csv --limit 100

	"""
	parser = argparse.ArgumentParser(description='Run user\'s time detection main process')
	parser.add_argument('handler', help='Main porpose handler')
	parser.add_argument('--loader', choices=['csv', 'sql'], help='Select data loader of command (Default is csv)', default='csv')
	parser.add_argument('--log_type', choices=['file', 'console'], help='Select which log type you prefer, file or console? (Default is console)', default='console')
	parser.add_argument('--limit', type=int, help='Define limit for number of rows either in sql or csv (Default is unlimited)')

	parser.add_argument('--source', help='Source file path (Must be in csv format)')
	parser.add_argument('--destination', help='Destination file path and name (Must be in csv format)')
	parser.add_argument('--collection', help='Destination mongo collection')

	args = parser.parse_args()

	if args.log_type == 'file':
		logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', filename='main.log', level=logging.DEBUG)
	else:
		logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.DEBUG)

	try:
		handler = eval(args.handler)
		logging.info('Handler recieved')
	except Exception as e:
		logging.error("No proper handler found for input")
		return

	handler(
		loader=args.loader, limit=args.limit, 
		source=args.source, collection=args.collection,
		destination=args.destination
	)
	logging.info('{} ended successfully.'.format(args.handler))


if __name__ == "__main__":
	parse_arguments()